# Используем основную ОС для Java
FROM openjdk:11-jdk

# Добавляем директорию приложения
WORKDIR /app

# Копируем pom.xml и собираем зависимости от сервера Maven
COPY pom.xml .
RUN ./mvnw dependency:go-offline -B

# Копируем исходный код приложения
COPY src ./src

# Собираем приложение
RUN ./mvnw package -DskipTests

# Копируем файлы приложения в директорию приложения в контейнере
RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)

# Устанавливаем переменную среды для запуска приложения
ENV JAVA_OPTS=""

# Запускаем приложение
ENTRYPOINT ["java","-cp","app:app/lib/*","com.example.demo.DemoApplication"]